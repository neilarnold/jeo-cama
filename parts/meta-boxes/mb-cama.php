<?php
/*
Title: CAMA Details 
Post Type: cama
Context: normal
Priority: high
Order: 1
*/

piklist('field', array(
    'type' => 'text'
    ,'scope' => 'post_meta' // Not used for settings sections
    ,'field' => 'cama_tax_rate'
    ,'label' => __('Tax Rate')
    ,'help' => __('Decimal, between 0 and 1.')
    ,'attributes' => array(
        'class' => 'regular-text', 
        'placeholder' => 'ex: 0.01'
    )
    ,'validate' => array( array( 
		'type' => 'range'
		,'options' => array(
			'min' => 0
			,'max' => 1
		)
	))
));

piklist('field', array(
	'type' => 'datepicker'
	,'scope' => 'post_meta' // Not used for settings sections
	,'field' => 'cama_tax_due_date'
    ,'add_more' => true
	,'label' => 'Tax Due Date'
	,'description' => 'Click + to add more dates.'
	,'attributes' => array(
		'class' => 'text'
	)
	,'options' => array(
		'dateFormat' => 'M d, yy'
		,'firstDay' => '0'
	)
));

piklist('field', array(
	'type' => 'datepicker'
	,'scope' => 'post_meta' // Not used for settings sections
	,'field' => 'cama_commitment_date'
	,'label' => 'Commitment Date'
	,'attributes' => array(
		'class' => 'text'
	)
	,'options' => array(
		'dateFormat' => 'M d, yy'
		,'firstDay' => '0'
	)
));
  
piklist('field', array(
	'type' => 'radio'
	,'field' => 'cama_enable_view_map'
	,'label' => 'Enable "View Map Online"'
	,'value' => 'yes'
	,'list' => false
	,'choices' => array(
		'yes' => 'Yes'
		,'no' => 'No'
	)
));

piklist('field', array(
    'type' => 'text'
    ,'scope' => 'post_meta' // Not used for settings sections
    ,'field' => 'cama_folder'
    ,'label' => __('File Folder')
    ,'help' => __('All CAMA files for this Town will be found in this folder. FTP files to: /cama_files/[folder-name]')
    ,'attributes' => array(
        'class'	 => 'regular-text', 
        'placeholder' => 'ex: folder-name'
    )
));

piklist('field', array(
    'type' => 'text'
    ,'scope' => 'post_meta' // Not used for settings sections
    ,'field' => 'doc_prefix'
    ,'label' => __('Document Prefix')
    ,'help' => __('All town files should start with the same text.  Ex: If documents is: Greenwood_2009 R-19.PDF, type: Greenwood')
    ,'attributes' => array(
        'class'	 => 'regular-text', 
        'placeholder' => 'ex: town-name'
    )
));

piklist('field', array(
	'type' => 'file'
	,'field' => 'cama_database'
	,'scope' => 'post_meta'
	,'label' => __('CAMA Database','cama')
	,'options' => array(
		'basic' => true,
		'button' => 'Add Database',
	)
));