<?php
/*
Title: CAMA Town Docs
Description: Displays Files for the Current Town
*/

global $post;

$cama_post;
$city_slug = get_query_var('city', '');
$city_id = 0;
$city_name = '';

$property_id = get_query_var('property', '');

$is_cama = false;
$is_property = false;

if (is_singular('cama')) {
	$is_cama = true;
	$cama_post = $post;
}
if (!empty($property)) {
	$is_property = true;

	$args = array(
	  'name'        => $city_slug,
	  'post_type'   => 'cama',
	  'numberposts' => 1
	);
	$my_posts = get_posts($args);
	if ( $my_posts )
		$cama_post = $my_posts[0];
}

$city_slug = $cama_post->post_name;
$city_id = $cama_post->ID;

$doc_prefix = get_post_meta($cama_post->ID, 'doc_prefix', true);
$cama_folder = Database_Utils::$cama_base_folder . get_post_meta($cama_post->ID, 'cama_folder', true);
$full_cama_folder = $_SERVER['DOCUMENT_ROOT'] . $cama_folder . '/' . $doc_prefix . '*';

?>

<?php echo $before_widget; ?>

<?php echo $before_title . 'Tax Maps for Download' . $after_title; ?>

<?php
echo "<div class='town-docs'><ul>";
$town_docs = glob($full_cama_folder);
asort($town_docs);
//foreach(glob($full_cama_folder, null) as $file) {
foreach($town_docs as $file) {
    $pathinfo = pathinfo($file);
    echo "<li><a href='". $cama_folder . '/' . $pathinfo['basename'] . "' target='_blank'>" . str_replace('_', ' ', $pathinfo['filename']) . "</a></li>";
}
echo "</ul></div>";



?>

<?php echo $after_widget; ?>
