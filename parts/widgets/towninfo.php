<?php
/*
Title: CAMA Town Info
Description: Displays Town Info for the Current Town
*/

global $post;

$cama_post;
$city_slug = get_query_var('city', '');
$city_id = 0;
$city_name = '';

$property_id = get_query_var('property', '');

$is_cama = false;
$is_property = false;

if (is_singular('cama')) {
	$is_cama = true;
	$cama_post = $post;
}
if (!empty($property)) {
	$is_property = true;

	$args = array(
	  'name'        => $city_slug,
	  'post_type'   => 'cama',
	  'numberposts' => 1
	);
	$my_posts = get_posts($args);
	if ( $my_posts )
		$cama_post = $my_posts[0];
}

$city_slug = $cama_post->post_name;
$city_id = $cama_post->ID;
$city_name = $cama_post->post_title;

$cama_tax_rate = get_post_meta($cama_post->ID, 'cama_tax_rate', true);
$cama_tax_due_date = get_post_meta($cama_post->ID, 'cama_tax_due_date', false);
$cama_commitment_date = get_post_meta($cama_post->ID, 'cama_commitment_date', true);


$cama_town = new Cama_Town;
if ($cama_town->open($cama_post->ID)) {
	$cama_town->loadTown();
} else {
	die();
}


?>

<?php echo $before_widget; ?>

<?php echo $before_title . 'Town Information' . $after_title; ?>

<p><b>Town of <?=$city_name?> </b></p>
<p>
	Tax Rate: <?=number_format($cama_town->row["PropertyTaxRate"],6)?><br />
	Tax Due Dates:
	<?=($cama_town->row["TaxDueDate1"] != '' ? date("m/d/Y", strtotime($cama_town->row["TaxDueDate1"])) : '')?><?=($cama_town->row["TaxDueDate2"] != '' ? ', ' . date("m/d/Y", strtotime($cama_town->row["TaxDueDate2"])) : '')?><?=($cama_town->row["TaxDueDate3"] != '' ? ', ' . date("m/d/Y", strtotime($cama_town->row["TaxDueDate3"])) : '')?><?=($cama_town->row["TaxDueDate4"] != '' ? ', ' . date("m/d/Y", strtotime($cama_town->row["TaxDueDate4"])) : '')?>
	<br />
	Commitment Date: <?=($cama_town->row["CommitmentDate"] != '' ? date("m/d/Y", strtotime($cama_town->row["CommitmentDate"])) : '')?>
</p>
<hr />
<p>
	<?=$cama_town->row["MailAddress"]?><br />
	<?=$cama_town->row["MailCity"]?>, <?=$cama_town->row["MailState"]?> <?=$cama_town->row["MailZip"]?><br />
	Phone: <?=$cama_town->row["TownPhone"]?><br />
	Fax: <?=$cama_town->row["TownFax"]?><br />
	Tax Collector: <?=$cama_town->row["TaxCollector"]?><br />
	Treasurer: <?=$cama_town->row["Treasurer"]?><br />
</p>




<?php

//echo 'Town ID: ' . $city_id . "<br />";
//echo 'Town Slug: ' . $city_slug . "<br />";

//if ($is_property) {
//	echo 'Property Key: ' . $property . "<br />";
//}

echo '<pre style="display:none;">';
print_r($cama_post);
echo '</pre>';
?>

<?php echo $after_widget; ?>
