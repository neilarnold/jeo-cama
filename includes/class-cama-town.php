<?php
/*
 * Access Database Utils
 * Version: 0.1
 *
 * Various tools for working with Access databases
 *
 */

 if (!defined('ABSPATH')) exit; // Exit if accessed directly


if (!class_exists('Cama_Town')) {
	class Cama_Town
	{

		public $db_file;
		public $db;
		public $cityId = 0;
		public $row;
        public $enable_view_map = 'no';

		public function open($cityId) {
			$this->cityId = $cityId;

            $this->enable_view_map = get_post_meta($this->cityId, 'cama_enable_view_map', true);
            $cama_database = get_post_meta($this->cityId, 'cama_database', false);
			$db_path = get_attached_file($cama_database[0]);

			if (!file_exists($db_path))
				return false;
			$this->db_file = $db_path;
            $this->db = new SQLite3($this->db_file);
			return true;
		}

		public function loadTown() {
			$qry = 'SELECT * FROM Town';	// Should only be 1 record in CAMA Database
			$results = $this->db->query($qry);

			if (count($results) > 0) {
				$this->row = $results->fetchArray();
			} else {
				$this->row = null;
			}

		}
	}
}
