<?php
/*
 * Access Database Utils
 * Version: 0.1
 *
 * Various tools for working with Access databases
 *
 */
 
 if (!defined('ABSPATH')) exit; // Exit if accessed directly
 
 
if (!class_exists('Cama_Property')) {
	class Cama_Property
	{
		public $db_file;
		public $db;
		public $cityId = 0;
		public $row;
		public $images = array();
		public $documents = array();
		public $land = array();
		public $site = array();
		public $building = array();
		public $visit_history = array();
		public $exemptions = array();
		
		public function open($cityId) {
			$this->cityId = $cityId;
				
			$cama_database = get_post_meta($this->cityId, 'cama_database', false);
			$db_path = get_attached_file($cama_database[0]);
		
			if (!file_exists($db_path))
				return false;
			$this->db_file = $db_path;
            $this->db = new SQLite3($this->db_file);
			return true;
		}
		
		public function loadProperty($key, $includeFiles = false) {
			$qry = 'SELECT * FROM Property WHERE (CurrentOwner = 1) AND (Key = ?)';
			$stmt = $this->db->prepare($qry);
			$stmt->bindValue(1, $key, SQLITE3_TEXT);
			$results = $stmt->execute();
			
			if (count($results) > 0) {
				$this->row = $results->fetchArray();
			} else
				$this->row = null;
				
			if ($includeFiles)
				$this->loadFiles();
		}
		
		public function loadFiles() {
			if ($this->row != null) {
				$doc_prefix = get_post_meta($this->cityId, 'doc_prefix', true);
				$cama_folder = Database_Utils::$cama_base_folder . get_post_meta($this->cityId, 'cama_folder', true);
				$full_cama_folder = $_SERVER['DOCUMENT_ROOT'] . $cama_folder . '/'; // . $doc_prefix . '*';
				
				$this->images = glob($full_cama_folder . $this->row['Key'] . '{.,_*.}{jpg,JPG,gif,png}', GLOB_BRACE|GLOB_NOSORT);
				$this->documents = glob($full_cama_folder . $this->row['Key'] . '{.*,_*.*}', GLOB_BRACE|GLOB_NOSORT);
				//$this->documents = glob($full_cama_folder . $doc_prefix . '*', GLOB_NOSORT);
				$this->documents = array_diff($this->documents, $this->images);

				
				$this->images = $this->cleanUpFiles($this->images, $cama_folder);
				$this->documents = $this->cleanUpFiles($this->documents, $cama_folder);
				
			}
		}
		
		private function cleanUpFiles($files, $folder) {
			$cleaned = array();
			foreach($files as $file) {
				$pathinfo = pathinfo($file);
				$cleaned[$folder . '/' . $pathinfo['basename']] = str_replace('_', ' ', $pathinfo['filename']);
			}
			return $cleaned;
		}
		
		public function loadLand() {

		
		
			$propertyKey = $this->row['PropertyKey'];
			$qry = 'SELECT TypeName, Size, Unit, Value, Notes, ' .
			       'ValueType, AccessAdj, RestrictedUseAdj, LocationAdj, ShapeAdj, ExcessFrAdj, DepthAdj, TopographyAdj, FrQualityAdj, MarketAdj, UtilitiesAdj, ' .
					'(SELECT TypeName FROM lt_LandTypes WHERE GroupNumber = LandParcels.[Group] AND TypeNumber = 0) AS Category ' .
					'FROM [LandParcels] '.
					'LEFT OUTER JOIN lt_LandTypes ON (LandParcels.[Group] = lt_landTypes.GroupNumber) AND (LandParcels.Type = lt_landTypes.TypeNumber) '.
					'WHERE PropertyKey = ? ' .
					'ORDER BY TypeName';
			$stmt = $this->db->prepare($qry);
			$stmt->bindValue(1, $propertyKey, SQLITE3_TEXT);
			$results = $stmt->execute();
			
			if (count($results) > 0) {
				$this->land = $results; //->fetchArray();
			} else
				$this->land = null;
		}	// loadLand()
		
		public function loadSite() {
			$propertyKey = $this->row['PropertyKey'];
			$qry = 'SELECT S.*, ' .
					'(SELECT Description FROM Codes C WHERE C.Code = S.SiteImprovementType LIMIT 0,1) AS Description ' .
					'FROM SiteImprovements S ' .
					'WHERE PropertyKey = ? ' .
					'ORDER BY SiteImprovementType';
			$stmt = $this->db->prepare($qry);
			$stmt->bindValue(1, $propertyKey, SQLITE3_TEXT);
			$results = $stmt->execute();
			
			if (count($results) > 0) {
				$this->site = $results;
			} else
				$this->site = null;
		}	// loadSite()
		
		public function loadBuilding() {
			$propertyKey = $this->row['PropertyKey'];
			$qry = 'SELECT B.*, BT.Description AS CategoryName ' .
					'FROM Buildings B LEFT OUTER JOIN lt_BuildingTypes BT  ' .
					'ON B.Category = BT.Category AND B.Style = BT.Style ' .
					'WHERE (PropertyKey = ?) ' .
					'ORDER BY BuildingKey';
			$stmt = $this->db->prepare($qry);
			$stmt->bindValue(1, $propertyKey, SQLITE3_TEXT);
			$results = $stmt->execute();
			
			if (count($results) > 0) {
				$this->building = $results;
			} else
				$this->building = null;
		}	// loadBuilding()
		
		public function loadVisitHistory() {
			$propertyKey = $this->row['PropertyKey'];
			$qry = 'SELECT V.*, A.AssessorName, ' .
					'(SELECT Description FROM Codes C WHERE C.Code = V.Purpose LIMIT 0,1) AS PurposeDesc, ' .
					'(SELECT Description FROM Codes C WHERE C.Code = V.Result LIMIT 0,1) AS ResultDesc ' .
					'FROM VisitHistory V ' .
					'LEFT OUTER JOIN lt_Assessors A ON V.AssessorKey = A.AssessorKey ' .
					'WHERE PropertyKey = ? ' .
					'ORDER BY VisitDate DESC';
			$stmt = $this->db->prepare($qry);
			$stmt->bindValue(1, $propertyKey, SQLITE3_TEXT);
			$results = $stmt->execute();
			
			if (count($results) > 0) {
				$this->visit_history = $results;
			} else
				$this->visit_history = null;
		}	// loadVisitHistory()
		
		public function loadExemptions() {
			$propertyKey = $this->row['PropertyKey'];
			$qry = 'SELECT E.*, L.ExemptCat, L.Value ' .
					'FROM Exemptions E ' .
					'LEFT OUTER JOIN lt_Exemptions L ON E.Type = L.TypeNumber ' .
					'WHERE E.PropertyKey = ? ' .
					'ORDER BY ExemptionKey';
			$stmt = $this->db->prepare($qry);
			$stmt->bindValue(1, $propertyKey, SQLITE3_TEXT);
			$results = $stmt->execute();
			
			if (count($results) > 0) {
				$this->exemptions = $results;
			} else
				$this->exemptions = null;
		}	// loadSite()
		
	}
}