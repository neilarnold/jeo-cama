<?php
/*
 * Access Database Utils
 * Version: 0.1
 *
 * Various tools for working with Access databases
 *
 */
 
 if (!defined('ABSPATH')) exit; // Exit if accessed directly
 
 
if (!class_exists('Database_Utils')) {
	class Database_Utils
	{
		public static $cama_base_folder = '/cama_files/';
	
		public $cama_post_id = 0;
		public $db_file = 'nope';
		public $db;
		
		private $field_list = 'Key, OwnerName1, StreetNumber, StreetName, LandValue, BuildingValue, BoughtFor, OwnerSince, PrivateData, (SELECT OptOut_Option from Town) AS OptOut_Option';
		
		public function db_exists() {
			return file_exists($this->db_file);
		}
		public function open() {
            $this->db = new SQLite3($this->db_file);
		}
		
		public function searchAll() {
			$qry = 'SELECT ' . $this->field_list . ' FROM Property WHERE (CurrentOwner = 1) AND NOT (OptOut_Option = 1 AND PrivateData = 1)  ORDER BY SortKey1, SortKey2, SortKey3, SortKey4';
			return $this->db->query($qry);
		}
		
		public function searchByMap($params) {
			
			$qry = 'SELECT ' . $this->field_list . ' FROM Property WHERE (CurrentOwner = 1) AND NOT (OptOut_Option = 1 AND PrivateData = 1)  ';
			
			reset($params);
			while (list($key, $val) = each($params))
				$qry .= "AND (" . $key . " = :" . $key . " COLLATE NOCASE) ";
			
			$qry .= 'ORDER BY SortKey1, SortKey2, SortKey3, SortKey4';
			$stmt = $this->db->prepare($qry);
			
			reset($params);
			while (list($key, $val) = each($params))
				$stmt->bindValue(':' . $key, $val, getArgType($val));

			$results = $stmt->execute();
			return $results;
		}
		
		public function searchByOwner($params) {
			
			$qry = 'SELECT ' . $this->field_list . ' FROM Property WHERE (CurrentOwner = 1) AND NOT (OptOut_Option = 1 AND PrivateData = 1)  ';
			$where = '';
			
			reset($params);
			while (list($key, $val) = each($params)) {
				if ($where != '')
					$where .= 'OR ';
				$where .= "(" . $key . " LIKE :" . $key . ") ";
			}
			if ($where != '')
				$qry .= 'AND (' . $where . ') ';
			
			$qry .= 'ORDER BY SortKey1, SortKey2, SortKey3, SortKey4';
			$stmt = $this->db->prepare($qry);
			
			reset($params);
			while (list($key, $val) = each($params))
				$stmt->bindValue(':' . $key, '%' . $val . '%', getArgType($val));

			$results = $stmt->execute();
			return $results;
		}
		
		public function searchByStreet($params) {
			
			$qry = 'SELECT ' . $this->field_list . ' FROM Property WHERE (CurrentOwner = 1) AND NOT (OptOut_Option = 1 AND PrivateData = 1)  ';
			$where = '';
			
			reset($params);
			while (list($key, $val) = each($params)) {
				if ($where != '')
					$where .= 'OR ';
				$where .= "(" . $key . " LIKE :" . $key . ") ";
			}
			if ($where != '')
				$qry .= 'AND (' . $where . ') ';
			
			$qry .= 'ORDER BY OwnerName1, OwnerName2';
			$stmt = $this->db->prepare($qry);
			
			reset($params);
			while (list($key, $val) = each($params))
				$stmt->bindValue(':' . $key, '%' . $val . '%', getArgType($val));

			$results = $stmt->execute();
			return $results;
		}
		
		public function searchByFilename($filename, $filetypes) {
			
			$search_str = '{';
			foreach ($filetypes as $filetype)
				$search_str .= '*' . $filetype . '*, ';
			if ($filename != '')
				$search_str .= '*' . $filename . '*';
			
			$search_str .= '}';
			
			$cama_folder = Database_Utils::$cama_base_folder . get_post_meta($this->cama_post_id, 'cama_folder', true);
			$full_cama_folder = $_SERVER['DOCUMENT_ROOT'] . $cama_folder . '/';
			$files_found = glob($full_cama_folder . $search_str, GLOB_BRACE | GLOB_NOSORT);
			
			
			
			$where_clause = "";
			foreach ($files_found as $file) {
				$pathinfo = pathinfo($file);
				$parts = explode("_", $pathinfo['filename']);
				$key = $parts[0];
				
				if ($where_clause != "")
					$where_clause .= ",";
				$where_clause .= "'" . $key . "'";
			}
			
			$qry = 'SELECT ' . $this->field_list . ' FROM Property WHERE (CurrentOwner = 1) AND NOT (OptOut_Option = 1 AND PrivateData = 1)  ';
			$qry .= 'AND (Key IN (' . $where_clause . ')) ';
			$qry .= 'ORDER BY OwnerName1, OwnerName2';
			$results = $this->db->query($qry);
			return $results;
			
		}
		
		public function getStreets($selected) {
			
			$qry = 'SELECT DISTINCT StreetName FROM Property WHERE StreetName IS NOT NULL ORDER BY StreetName';
			
			$results = $this->db->query($qry);
			
			
			while ($row = $results->fetchArray()) {
				$sel = "";
				if ($row["StreetName"] == $selected)
					$sel = " selected='selected'";
				echo '<option value="' . $row["StreetName"] . '"' . $sel . '>' . $row['StreetName'] . '</option>';	
			}
		}
		
		public function getProperty($key) {
			
			$qry = 'SELECT * FROM Property WHERE (CurrentOwner = 1) AND NOT (OptOut_Option = 1 AND PrivateData = 1)  AND (Key = ?)';
			$stmt = $this->db->prepare($qry);
			$stmt->bindValue(1, $key, SQLITE3_TEXT);
			$results = $stmt->execute();
			
			if (count($results) > 0) {
				$results = $results->fetchArray();
				return $results;
			} else {
				return false;
			}
		}
		
		public function getTownInfo() {
			// Should only be 1 record in CAMA Database
			$qry = 'SELECT * FROM Town';
			$results = $this->db->query($qry);
			
			if (count($results) > 0) {
				$results = $results->fetchArray();
				return $results;
			} else {
				return false;
			}
		}
		
		// Static Functions --- --- ---
		public static function get_cama_post($cama_slug) {
			$args = array(
				'name'        => $cama_slug,
				'post_type'   => 'cama',
				'numberposts' => 1
			);
			$my_posts = get_posts($args);
			if ( $my_posts )
				return $my_posts[0];	
		}
		
		public static function Show($label, $value) {
			if ($value != '')
				return '<p>' . $label . ': <b>' . $value . '</b></p>';
			return '';
		}
		
		public static function ShowMoney($label, $value, $format) {
			if ($value != '')
				return '<p>' . $label . ': <b>' . money_format('%.2n', $value) . '</b></p>';
			return '';
		}
		
		public static function ShowDate($label, $value) {
			if ($value != '')
				return '<p>' . $label . ': <b>' .  date("m/d/Y", strtotime($value)) . '</b></p>';
			return '';
		}
		
		public static function ShowAddress($row) { //$address1, $address2, $city, $state, $zip) {
			// Special Case for addresses
			$ret = '';
			if ($row['AddressLine1'] != '') {
				$ret .= '<p>Mailing Address:ss<br /><b>';
				$ret .= $row['AddressLine1'];
				if ($row['AddressLine2'] != '')
					$ret .= '<br />' . $row['AddressLine2'];
				if ($row['City'] != '')
					$ret .= '<br />' . $row['City'] . ', ' . $row['State'] . ' ' . $row['ZIP'];
				$ret .= '</b></p>';
			}
			
			return $ret;
		}
		
		// END - Static Functions --- --- ---
		
	}
	

}	// if (!class_exists('Database_Utils')) {

function getArgType($arg)
{
    switch (gettype($arg))
    {
        case 'double': return SQLITE3_FLOAT;
        case 'integer': return SQLITE3_INTEGER;
        case 'boolean': return SQLITE3_INTEGER;
        case 'NULL': return SQLITE3_NULL;
        case 'string': return SQLITE3_TEXT;
        default:
            throw new \InvalidArgumentException('Argument is of invalid type '.gettype($arg));
    }
}

function isChecked($checkName, $checkValue) {
	if(isset($_POST[$checkName]) && is_array($_POST[$checkName]) && in_array($checkValue, $_POST[$checkName])) 
		return 'checked="checked"';
}
