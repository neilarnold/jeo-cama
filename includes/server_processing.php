<?php

try {
    // Set default timezone
    date_default_timezone_set('Europe/Rome');
    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Easy set variables
     */
    
    /* Array of database columns which should be read and sent back to DataTables. Use a space where
     * you want to insert a non-database field (for example a counter or static image)
     */
    $aColumns = array( 'Key', 'OwnerName1', 'StreetNumber', 'StreetName', 'LandValue', 'BuildingValue', 'BoughtFor', 'OwnerSince' );
    
    /* Indexed column (used for fast and accurate table cardinality) */
    $sIndexColumn = "Key";
    
    /* DB table to use */
    $sTable = "Property";
    
    /* Database connection information */
    $gaSql['path']       = "/home/wp_rnez7d/newsite.jeodonnell.com/wp-content/uploads/2015/06/Greenwood.Cama_.db";
    $gaSql['password']   = "";
    $file_db  = null;
    
    // Create (connect to) SQLite database in file
    $file_db = new PDO('sqlite:'.$gaSql['path']);
    // Set errormode to exceptions
    $file_db->setAttribute(PDO::ATTR_ERRMODE,
                            PDO::ERRMODE_EXCEPTION);
    /*
     * Paging
     */
    $sLimit = "";
	if ( isset( $_GET['length'] ) && $_GET['length'] != '-1' )
    {
        $sLimit = "LIMIT ".intval( $_GET['start'] ).", ".
          intval( $_GET['length'] );
    }
    
    
    /*
     * Ordering
     */
    $sOrder = "";
	//echo "{order: " .  print_r($_GET['order']) . '}';
	
    if ( isset( $_GET['order'][0]['column'] ) )
    {
        $sOrder = "ORDER BY  ";
        for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
        {
            if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
            {
                $sOrder .= "`".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."` ".
                  ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
            }
        }
        
        $sOrder = substr_replace( $sOrder, "", -2 );
        if ( $sOrder == "ORDER BY" )
        {
            $sOrder = "";
        }
    }
    
    
    /*
     * Filtering
     * NOTE this does not match the built-in DataTables filtering which does it
     * word by word on any field. It's possible to do here, but concerned about efficiency
     * on very large tables
     */
    $sWhere = "";
    if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
    {
        $sWhere = "WHERE (";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            $sWhere .= "`".$aColumns[$i]."` LIKE ".$file_db->quote( '%'. $_GET['sSearch'] . '%' )." OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
    }
    
    /* Individual column filtering */
    for ( $i=0 ; $i<count($aColumns) ; $i++ )
    {
        if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
        {
            if ( $sWhere == "" )
            {
                $sWhere = "WHERE ";
            }
            else
            {
                $sWhere .= " AND ";
            }
            $sWhere .= "`".$aColumns[$i]."` LIKE ".$file_db->quote( '%'. $_GET['sSearch_'.$i] . '%' )." ";
        }
    }
    
    /*
     * SQL queries
     * Get data to display
     */
    
    /* Data set length after filtering */   
    $sQuery = "
      SELECT COUNT(`".$sIndexColumn."`) AS counter
      FROM   $sTable
      $sWhere
      $sOrder
      ";
    $rResult = $file_db->query($sQuery)->fetch(PDO::FETCH_ASSOC);
    $iFilteredTotal = $rResult['counter'];
    
    
    /* Total data set length */
    $sQuery = "
      SELECT COUNT(`".$sIndexColumn."`) AS counter
      FROM   $sTable";
    $rResult = $file_db->query($sQuery)->fetch(PDO::FETCH_ASSOC);
    $iTotal = $rResult['counter'];
    
    /*
     * Output
     */
    $output = array(
      "sEcho" => intval(@$_GET['sEcho']),
      "iTotalRecords" => $iTotal,
      "iTotalDisplayRecords" => $iFilteredTotal,
      "aaData" => array()
    );
    
    /* Data */
    $sQuery = "
      SELECT ".str_replace(" , ", " ", implode(", ", $aColumns))."
      FROM   $sTable
      $sWhere
      $sOrder
      $sLimit";
    
    $rResult = $file_db->query($sQuery);
    while ( $aRow = $rResult->fetch(PDO::FETCH_ASSOC) )
    {
        $row = array();
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            /* if $aColumns[$i] is space is not data */
            if ( $aColumns[$i] != ' ' )
            {
                $row[] = $aRow[ $aColumns[$i] ];
            }
        }
        $output['aaData'][] = $row;
    }
    
    echo json_encode( $output );
}
catch ( Exception $e )
{
    header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
    die( $e->getMessage() );
}
?>