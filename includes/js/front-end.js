﻿
$j = jQuery.noConflict();

$j(document).ready(
  function () {

  	$j('.datatable').DataTable({
  		searching: false,
		ordering:false,
  		responsive: true,
  		pageLength: 25,
  		lengthChange: false,
  	});

  	$j('.datatable-ss').DataTable({
  		processing: true,
  		serverSide: true,
  		responsive: true,
  		ajax: "/wp-content/plugins/jeo-cama/includes/server_processing.php"
  	});
  	if ( $j( ".datatable-json" ).length )
  	$j('.datatable-json').DataTable({
  		searching: false,
  		responsive: true,
  		pageLength: 25,
  		lengthChange: false,
  		data: json_data,
		columns: [
			{ data: 'Key' },
			{ data: 'OwnerName1' },
			{ data: 'StreetNumber' },
			{ data: 'StreetName' },
			{ data: 'LandValue', className: 'align-right', type: 'num-fmt' },
			{ data: 'BuildingValue', className: 'align-right', type: 'num-fmt' },
			{ data: 'BoughtFor', className: 'align-right', type: 'num-fmt' },
			{ data: 'OwnerSince', type: 'date' }
		],
		columnDefs: [
			{
				targets: 0,
				render: function (data, type, row) {
					return '<a href="./' + data + '">' + data + '</a>';
				}
			},
			{
				targets: [4,5,6 ],
				render: function (data, type, row) {
					if (!isNaN(data) && data != null)
						return '$' + data.formatMoney(2);
					return data;
				}
			},
			{
				targets: [7],
				render: function (data, type, row) {
					if (data != null)
						return fortmatDate(data);
					return data;
				}
			},

		]
  	});


  	if (typeof $_POST !== 'undefined') {
  		$active_tab = $_POST["cama-search-tab"];
  		if ($active_tab == '' || $active_tab == undefined)
  			$active_tab = 0;

  		$j("ul#tabs li").removeClass("active");
  		$j("ul#tab li").removeClass("active");

  		$j('ul#tabs li:eq(' + $active_tab + ')').addClass("active");
  		$j('ul#tab li:eq(' + $active_tab + ')').addClass("active");
  	}


  	$j("ul#tabs li").click(function (e) {
  		if (!$j(this).hasClass("active")) {
  			var tabNum = $j(this).index();
  			var nthChild = tabNum + 1;
  			$j("ul#tabs li.active").removeClass("active");
  			$j(this).addClass("active");
  			$j("ul#tab li.active").removeClass("active");
  			$j("ul#tab li:nth-child(" + nthChild + ")").addClass("active");
  		}
  	});


  	$j('.photo-carousel').slick({
  		dots: true,
  		infinite: true,
  		speed: 300,
  		slidesToShow: 1,
  		centerMode: true,
  		variableWidth: true
  	});

  	//$j("#owl-example").owlCarousel({
  	//	autoPlay: false,
  	//	stopOnHover: true,
  	//	navigation: true,
  	//	paginationSpeed: 1000,
  	//	goToFirstSpeed: 2000,
  	//	singleItem: true,
  	//	autoHeight: true,
  	//});



  }
);


function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regexS = "[\\?&]" + name + "=([^&#]*)";
	var regex = new RegExp(regexS);
	var results = regex.exec(window.location.search);
	if (results == null)
		return "";
	else
		return decodeURIComponent(results[1].replace(/\+/g, " "));
}

Number.prototype.formatMoney = function (c, d, t) {
	var n = this,
		c = isNaN(c = Math.abs(c)) ? 2 : c,
		d = d == undefined ? "." : d,
		t = t == undefined ? "," : t,
		s = n < 0 ? "-" : "",
		i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
		j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function fortmatDate(input) {

	if (moment(input).isValid()) {
		return moment(input).format('L');
	}
	return '';
}
