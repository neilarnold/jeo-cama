<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Cama Template
 *
Template Name:  Cama Detail Page
 *
 * @file           cama-member.php
 * @author         Neil Arnold
 * @copyright      2015 Neil Arnold
 * @version        Release: 1.0
 * @filesource     wp-content/plugins/jeo-cama/templates/single-cama.php
 */

//global $post;

$cama_tax_rate = get_post_meta($post->ID, 'cama_tax_rate', true);
$cama_tax_due_date = get_post_meta($post->ID, 'cama_tax_due_date', false);
$cama_commitment_date = get_post_meta($post->ID, 'cama_commitment_date', true);
$cama_enable_view_map = get_post_meta($post->ID, 'cama_enable_view_map', true);
$cama_database = get_post_meta($post->ID, 'cama_database', false);

$db_link = wp_get_attachment_link($cama_database[0]);
$db_path = get_attached_file($cama_database[0]);

$db_utils = new Database_Utils;
$db_utils->db_file = $db_path;
$db_utils->cama_post_id = $post->ID;

if ($db_utils->db_exists()) {
	$db_utils->open();
}

$cama_town = new Cama_Town;
if ($cama_town->open($post->ID)) {
	$cama_town->loadTown();
}
$optout_option = $cama_town->row['OptOut_Option'];
$hide_name = ($optout_option == 2 && $cama_town->row['OptOut_Mask_OwnerName']);
$hide_price = ($optout_option == 2 && $cama_town->row['OptOut_Mask_PurchasePrice']);


get_header(); ?>

<div class="x-container max width offset">
<div class="<?php x_main_content_class(); ?>" role="main">

    <?php while ( have_posts() ) : the_post(); ?>

        <?php the_content(); ?>

		<div id="cama-search" class="tabs">
		  <ul id="tabs">
			<li class="active">Map / Lot / Sub / Type</li>
			<li>Owner Name</li>
			<li>All</li>
			<li>Street</li>
			<li>Documents</li>
		  </ul>
		  <ul id="tab">
			  <li class="active">
				<form action='<?php echo get_permalink(); ?>' method='post'>
				<input type="hidden" name="cama-search-tab" id="cama-search-tab" value="0" />
				<div class="x-column x-sm x-1-5">
					<input type="text" name="cama-search-map" value="<?=$_POST['cama-search-map']?>" placeholder="Map" />
				</div>
				<div class="x-column x-sm x-1-5">
					<input type="text" name="cama-search-lot" value="<?=$_POST['cama-search-lot']?>" placeholder="Lot" />
				</div>
				<div class="x-column x-sm x-1-5">
					<input type="text" name="cama-search-sub" value="<?=$_POST['cama-search-sub']?>" placeholder="Sub" />
				</div>
				<div class="x-column x-sm x-1-5">
					<input type="text" name="cama-search-type" value="<?=$_POST['cama-search-type']?>" placeholder="Type" />
				</div>
				<div class="x-column x-sm x-1-5 last">
					<input type="submit" name="cama-submit-map" value="SEARCH" />
				</div>
				<hr class="x-clear">
				</form>
			  </li>
			  <li>
				<form action='<?php echo get_permalink(); ?>' method='post'>
				<input type="hidden" name="cama-search-tab" id="cama-search-tab" value="1" />
				<div class="x-column x-sm x-4-5">
					<input type="text" name="cama-search-ownername" value="<?=$_POST['cama-search-ownername']?>" placeholder="Owner Name" />
				</div>
				<div class="x-column x-sm x-1-5 last">
					<input type="submit" name="cama-submit-ownername" value="SEARCH" />
				</div>
				<hr class="x-clear">
				</form>
			  </li>
			  <li>
				<form action='<?php echo get_permalink(); ?>' method='post'>
				<input type="hidden" name="cama-search-tab" id="cama-search-tab" value="2" />
				<div class="x-column x-sm x-4-5">
					This search will return all properties in CAMA.
				</div>
				<div class="x-column x-sm x-1-5 last">
					<input type="submit" name="cama-submit-all" value="GET ALL" />
				</div>
				<hr class="x-clear">
				</form>
			  </li>
			  <li>
				<form action='<?php echo get_permalink(); ?>' method='post'>
				<input type="hidden" name="cama-search-tab" id="cama-search-tab" value="3" />
				<div class="x-column x-sm x-4-5">
					<select name="cama-search-street">
						<option value="">Select a Street Name</option>
						<?php $db_utils->getStreets($_POST['cama-search-street']) ?>
					</select>
				</div>
				<div class="x-column x-sm x-1-5 last">
					<input type="submit" name="cama-submit-street" value="SEARCH" />
				</div>
				<hr class="x-clear">
				</form>
			  </li>
			  <li>
				<form action='<?php echo get_permalink(); ?>' method='post'>
				<input type="hidden" name="cama-search-tab" id="cama-search-tab" value="4" />
				<div class="x-column x-sm x-2-5">
					<input type="checkbox" name="cama-search-filetype[]" <?=isChecked('cama-search-filetype', '_RETT'); ?> value="_RETT" /> Real Estate Tax Form (_RETT)<br />
					<input type="checkbox" name="cama-search-filetype[]" <?=isChecked('cama-search-filetype', '_TG'); ?> value="_TG" /> Tree Growth Application (_TG)<br />
					<input type="checkbox" name="cama-search-filetype[]" <?=isChecked('cama-search-filetype', '_BP'); ?> value="_BP" /> Building Permit (_BP)<br />
					<input type="checkbox" name="cama-search-filetype[]" <?=isChecked('cama-search-filetype', '_Sketch'); ?> value="_Sketch" /> Building Sketch (_Sketch)<br />
					<input type="checkbox" name="cama-search-filetype[]" <?=isChecked('cama-search-filetype', '_SD'); ?> value="_SD" /> Septic Design (_SD)<br />
					<input type="checkbox" name="cama-search-filetype[]" <?=isChecked('cama-search-filetype', '_Homestead'); ?> value="_Homestead" /> Homestead Application (_Homestead)<br />
					<input type="checkbox" name="cama-search-filetype[]" <?=isChecked('cama-search-filetype', '_OS'); ?> value="_OS" /> Open Space Application (_OS)<br />
					<input type="checkbox" name="cama-search-filetype[]" <?=isChecked('cama-search-filetype', '_FL'); ?> value="_FL" /> Farmland Application (_FL)<br />
				</div>
				<div class="x-column x-sm x-2-5">
					<p><input type="text" name="cama-search-filename" placeholder="Filename" value="<?=$_POST['cama-search-filename']?>" /></p>
					<p><em>Note: This is a processor-intensive search and may take slightly longer to run.</em></p>
				</div>
				<div class="x-column x-sm x-1-5 last">
					<input type="submit" name="cama-submit-documents" value="SEARCH" />
				</div>
				<hr class="x-clear">
				</form>
			  </li>
			</ul>
		</div>

		<?php

        if ($db_utils->db_exists()) {

			$params = [];

			if (empty($_POST)) {
				$results = $db_utils->searchAll();
			} else {
				if (isset($_POST['cama-submit-map'])) {
					if (!empty($_POST['cama-search-map']))
						$params['Key1']	= $_POST['cama-search-map'];
					if (!empty($_POST['cama-search-lot']))
						$params['Key2']	= $_POST['cama-search-lot'];
					if (!empty($_POST['cama-search-sub']))
						$params['Key3']	= $_POST['cama-search-sub'];
					if (!empty($_POST['cama-search-type']))
						$params['Key4']	= $_POST['cama-search-type'];
					$results = $db_utils->searchByMap($params);
				}
				if(isset($_POST['cama-submit-ownername'])) {
					if (!empty($_POST['cama-search-ownername'])) {
						$params['OwnerName1'] = $_POST['cama-search-ownername'];
						$params['OwnerName2'] = $_POST['cama-search-ownername'];
					}
					$results = $db_utils->searchByOwner($params);
				}
				if(isset($_POST['cama-submit-all']))
					$results = $db_utils->searchAll();
				if(isset($_POST['cama-submit-street'])) {
					if (!empty($_POST['cama-search-street'])) {
						$params['StreetName'] = $_POST['cama-search-street'];
						$params['AddressLine1'] = $_POST['cama-search-street'];
					}
					$results = $db_utils->searchByStreet($params);
				}
				if(isset($_POST['cama-submit-documents'])) {
					if (!empty($_POST['cama-search-filename']))
						$filename = $_POST['cama-search-filename'];
					if (!empty($_POST['cama-search-filetype']))
						$filetypes = $_POST['cama-search-filetype'];

					$results = $db_utils->searchByFilename($filename, $filetypes);
					//$results = $db_utils->searchByStreet($params);

				}
				//$results = $db_utils->db->query('SELECT * FROM Property WHERE (CurrentOwner = 1) AND (PrivateData = 0) ORDER BY SortKey1, SortKey2, SortKey3, SortKey4');
			}

			$rows = array();
			while ($row = $results->fetchArray()) {
				if ($hide_name && $row["PrivateData"] == 1)
					$row["OwnerName1"] = '<em>Contact Town Office</em>';
				if ($hide_price && $row["PrivateData"] == 1)
					$row["BoughtFor"] = '<em>Contact Town Office</em>';
				$rows[] = $row;
			}
			$rows_json = json_encode($rows);

			echo '<script type="text/javascript">';
			echo 'json_data = ' . $rows_json;
			echo '</script>';

            echo '<table class="datatable-json responsive ">';
			echo '<thead>';
			echo '	<tr>';
			echo '	<th >Site</th>';
			echo '	<th >Owners</th>';
			echo '	<th >#</th>';
			echo '	<th ><abbr title="Street Address">Address</abbr></th>';
			echo '	<th >Land</th>';
            echo '	<th >Bulding</th>';
            echo '	<th ><abbr title="Sale Price">Price</abbr></th>';
            echo '	<th ><abbr title="Sale Price">Date</abbr></th>';
			echo '  </tr>';
			echo '</thead>';
			if (false) {
				echo '<tbody>';

				setlocale(LC_MONETARY, 'en_US.UTF-8');
				if ($results)
				while ($row = $results->fetchArray()) {
					echo '<tr>';
					echo '	<td>x' . ($row["PrivateData"] == true) . 'x<a href="./' . $row["Key"]. '/">' . $row["Key"] . '</a></td>';
					if ($hide_name && $row["PrivateData"] == 1)
						echo '	<td><em>Contact Town Office</em></td>';
					else
						echo '	<td>' . $row["OwnerName1"] . '</td>';
					echo '	<td>' . $row["StreetNumber"] . '</td>';
					echo '	<td>' . $row["StreetName"] . '</td>';
					echo '	<td>' . money_format('%.0n', $row["LandValue"]) . '</td>';
					echo '	<td>' . money_format('%.0n', $row["BuildingValue"]) . '</td>';
					if ($hide_price && $row["PrivateData"] == 1)
						echo '	<td><em>Contact Town Office</em></td>';
					else
						echo '	<td>' . money_format('%.0n', $row["BoughtFor"]) . '</td>';
					echo '	<td>' . ($row["OwnerSince"] != '' ? date("m/d/Y", strtotime($row["OwnerSince"])) : '') . '</td>';
					echo '</tr>';
					//var_dump($row);
					//echo $row["key"] + " | ";
				}
				echo '</tbody>';
			} // if false
		    echo '</table>';
        } else {
            echo 'Database Not Found';
        }





        ?>
        <br />
    <?php endwhile; ?>

    </div>
	<aside class="x-sidebar right" role="complementary">
		<?php dynamic_sidebar( 'sidebar-cama' ); ?>
	</aside>

  </div>

<?php get_footer(); ?>

<script type="text/javascript">
	var $_POST = <?php echo json_encode($_POST); ?>;
</script>
