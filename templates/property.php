<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

setlocale(LC_MONETARY, 'en_US');
define('CAMA_PROPERTY', true);
/**
 * Cama Template
 * Template Name:  Cama Detail Page
 *
 * @file           property.php
 * @author         Neil Arnold
 * @copyright      2015 Neil Arnold
 * @version        Release: 1.0
 * @filesource     wp-content/plugins/jeo-cama/templates/property.php
 */

global $post;

$city_slug = $wp_query->query_vars['city'];
$property_key = $wp_query->query_vars['property'];

$post = Database_Utils::get_cama_post($city_slug);

//$cama_database = get_post_meta($post->ID, 'cama_database', false);
//$db_path = get_attached_file($cama_database[0]);

//$db_utils = new Database_Utils;
//$db_utils->db_file = $db_path;

//echo "City Slug: " . $city_slug . "<br />";
//echo "Property Key: " . $property_key . "<br />";

$cama_property = new Cama_Property;
if ($cama_property->open($post->ID)) {
	$cama_property->loadProperty($property_key, true);
	$cama_property->loadLand();
	$cama_property->loadSite();
	$cama_property->loadBuilding();
	$cama_property->loadVisitHistory();
	$cama_property->loadExemptions();
} else {
	header('Location: /cama/' . $city_slug);
	die();
}

$cama_town = new Cama_Town;
if ($cama_town->open($post->ID)) {
	$cama_town->loadTown();
} else {
	header('Location: /cama/' . $city_slug);
	die();
}

// $optout_option = 1 = exclude all records where privatedata field in property table is true
// $optout_option = 2 = mask fields where privatedata in property table is true and other optout fields is/are true
$optout_option = $cama_town->row['OptOut_Option'];
$is_private = ($cama_property->row["PrivateData"] == 1);

$hide_name = ($optout_option == 2 && $is_private && $cama_town->row['OptOut_Mask_OwnerName']);
$hide_address = ($optout_option == 2 && $is_private && $cama_town->row['OptOut_Mask_MailingAddress']);
$hide_price = ($optout_option == 2 && $is_private && $cama_town->row['OptOut_Mask_PurchasePrice']);

//echo '$hide_name: ' . $hide_name . "<br />";
//echo '$hide_address: ' . $hide_address . "<br />";
//echo '$hide_price: ' . $hide_price . "<br />";

if (($optout_option == 1) && ($is_private)) {	// Whole record is private.
    header('Location: /cama/' . $city_slug);
    die();
}


//$property = $db_utils->getProperty($property_key);
//$town = $db_utils->getTownInfo();

//if (!$property) {
//    header('Location: /cama/' . $city_slug);
//    die();
//}




// BEGIN Page --- --- ---

get_header(); ?>
<a name="top"></a>
<div class="x-container max width offset">
<div class="<?php x_main_content_class(); ?>" role="main">

	<h4 class="h-widget"><?=$cama_property->row["StreetNumber"]?> <?=$cama_property->row["StreetName"]?>, <?=$cama_town->row["Town"]?>, <?=$cama_town->row["MailState"]?></h4>

	<div class="photo-carousel">
		<?php
        asort($cama_property->images);
		foreach($cama_property->images as $url => $title) {
			echo "<div><a href='$url' rel='lightbox'><img src='$url' alt='$title' /></a></div>";
		}
		?>
	</div>


	<div class="property-info property-box">
		<h5>Property Information</h5>
		<div class="x-column x-sm x-1-2">
			<p>
				Site: <b>
					<?=($cama_property->row["Key1"] != '' ? 'Map ' . $cama_property->row["Key1"] : '')?>
					<?=($cama_property->row["Key2"] != '' ? ', Lot ' . $cama_property->row["Key2"] : '')?>
					<?=($cama_property->row["Key3"] != '' ? ', Sub ' . $cama_property->row["Key3"] : '')?>
					<?=($cama_property->row["Key4"] != '' ? ', Type ' . $cama_property->row["Key4"] : '')?></b>
			</p>
			<p>Town: <b><?=$cama_town->row["Town"]?></b></p>
			<p>Tax Year: <b><?=$cama_town->row["Year"]?></b></p>
			<?php

			if ($hide_name)
				echo Database_Utils::show('Owner', '<em>Contact Town Office</em>');
			else
				echo Database_Utils::show('Owner', $cama_property->row['OwnerName1']);
			?>
			<p>Last Committed Tax: <b><?=money_format('%.2n', $cama_property->row["LastCommittedTax"]);?></b></p>
			<p>See: <b><?=$cama_property->row["See"];?></b></p>
			<p>Includes: <b><?=$cama_property->row["Includes"];?></b></p>

			<?php
			if ($cama_town->enable_view_map == 'yes') {
				echo '<p>Interactive Map: <a href="http://www.caigisonline.net/ActonME/?CamaIdName=Account_Number&CamaIdValue=' . $cama_property->row["TrioAccountNumber"] . '" target="_blank"><b>Click Here to View</b></a></p>';
			}
			?>
		</div>
		<div class="x-column x-sm x-1-2 last">
			<p>Land Value: <b><?=money_format('%.0n', $cama_property->row["LandValue"]);?></b></p>
			<p>Building Value: <b><?=money_format('%.0n', $cama_property->row["BuildingValue"]);?></b></p>
			<p>Total Real Value: <b><?=money_format('%.0n', ($cama_property->row["LandValue"] + $cama_property->row["BuildingValue"]));?></b></p>
			<p>Exemption Value: <b><?=money_format('%.0n', $cama_property->row["ExemptionValue"]);?></b></p>
			<p>Net Taxable Real Value: <b><?=money_format('%.0n', ($cama_property->row["LandValue"] + $cama_property->row["BuildingValue"])- $cama_property->row["ExemptionValue"]);?></b></p>
			<p>Personal Property: <b><?=money_format('%.0n', $cama_property->row["PersonalValue"]);?></b></p>
		</div>
		<hr class="x-clear">
	</div>	<!-- Property Information -->


	<div id="cama-property-details" class="property-details  property-box">
		<h5>Owner Information</h5>
		<div class="x-column x-sm x-1-3">
			<?php
			if ($hide_name) {
				echo Database_Utils::show('Owner', '<em>Contact Town Office</em>');
			} else {
				echo Database_Utils::show('Owner #1', $cama_property->row['OwnerName1']);
				echo Database_Utils::show('Owner #2', $cama_property->row['OwnerName2']);
			}

			if ($hide_address)
				echo Database_Utils::show('Mailing Address', '<em>Contact the Town</em>');
			else
				echo Database_Utils::ShowAddress($cama_property->row);

			echo Database_Utils::show('Phone Number', $cama_property->row['Phone1']);
			echo Database_Utils::show('Trio Account #', $cama_property->row['TrioAccountNumber']);
			?>

		</div>
		<div class="x-column x-sm x-1-3">
			<?php
			echo Database_Utils::show('Owner Since', ($cama_property->row["OwnerSince"] != '' ? date("m/d/Y", strtotime($cama_property->row["OwnerSince"])) : ''));
			echo Database_Utils::show('Book', $cama_property->row['Book']);
			echo Database_Utils::show('Page', $cama_property->row['bPage']);
			if ($hide_price)
				echo Database_Utils::show('Purchase Price', '<em>Contact the Town</em>');
			else
				echo Database_Utils::ShowMoney('Purchase Price', $cama_property->row['BoughtFor'], '%.0n');
			?>
		</div>
		<div class="x-column x-sm x-1-3 last">
			<p><b>Documents</b></p>
			<ul>
			<?php
			foreach($cama_property->images as $url => $title) {
				echo "<li><a href='$url' rel='lightbox'>$title</a></li>";
			}
			asort($cama_property->documents);
			foreach($cama_property->documents as $url => $title) {
				echo "<li><a href='$url' target='_blank'>$title</a></li>";
			}
			?>
			</ul>
		</div>
		<hr class="x-clear">
	</div>	<!-- Owner Information -->


	<div id="cama-land-details" class="property-box">
		<h5>Land Information</h5>
		<div class="x-column x-sm x-1-1">
			<table class="datatable responsive" data-paging="false" data-info="false" width="100%">
			<thead>
	        <tr>
	          <th>Land Group: Type</th>
	          <th class="align-right">Size</th>
	          <th>Method</th>
			  <th class="align-right">Value</th>
			  <th class="align-right">Total Adj</th>
			  <th>Adj Details</th>
	        </tr>
			</thead>
			<tbody>
			<?php
			$total_value = 0;
			$total_units = 0;
			while ($row = $cama_property->land->fetchArray()) {
				$total_value += $row['Value'];
				if (strtolower($row['Unit']) == 'ac')
					$total_units += $row['Size'];

				$adj_total = 1;
				$adj_detail = '';
				if ($row['AccessAdj'] != 1) {
					$adj_total	*= $row['AccessAdj'];
					$adj_detail .= 'Access: ' . sprintf("%.1f%%", $row['AccessAdj'] * 100) . '; ';
				}
				if ($row['RestrictedUseAdj'] != 1) {
					$adj_total	*= $row['RestrictedUseAdj'];
					$adj_detail .= 'Restricted Use ' . sprintf("%.1f%%", $row['RestrictedUseAdj'] * 100) . '; ';
				}
				if ($row['LocationAdj'] != 1) {
					$adj_total	*= $row['LocationAdj'];
					$adj_detail .= 'Location: ' . sprintf("%.1f%%", $row['LocationAdj'] * 100) . '; ';
				}
				if ($row['ShapeAdj'] != 1) {
					$adj_total	*= $row['ShapeAdj'];
					$adj_detail .= 'Shape: ' . sprintf("%.1f%%", $row['ShapeAdj'] * 100) . '; ';
				}
				if ($row['ExcessFrAdj'] != 1) {
					$adj_total	*= $row['ExcessFrAdj'];
					$adj_detail .= 'Excess Fr: ' . sprintf("%.1f%%", $row['ExcessFrAdj'] * 100) . '; ';
				}
				if ($row['DepthAdj'] != 1) {
					$adj_total	*= $row['DepthAdj'];
					$adj_detail .= 'Depth: ' . sprintf("%.1f%%", $row['DepthAdj'] * 100) . '; ';
				}
				if ($row['TopographyAdj'] != 1) {
					$adj_total	*= $row['TopographyAdj'];
					$adj_detail .= 'Topography: ' . sprintf("%.1f%%", $row['TopographyAdj'] * 100) . '; ';
				}
				if ($row['FrQualityAdj'] != 1) {
					$adj_total	*= $row['FrQualityAdj'];
					$adj_detail .= 'Fr Quality: ' . sprintf("%.1f%%", $row['FrQualityAdj'] * 100) . '; ';
				}
				if ($row['MarketAdj'] != 1) {
					$adj_total	*= $row['MarketAdj'];
					$adj_detail .= 'Market ' . sprintf("%.1f%%", $row['MarketAdj'] * 100) . '; ';
				}
				if ($row['UtilitiesAdj'] != 1) {
					$adj_total	*= $row['UtilitiesAdj'];
					$adj_detail .= 'Utilities ' . sprintf("%.1f%%", $row['UtilitiesAdj'] * 100) . '; ';
				}

				echo '<tr>';
				echo '<td>' . $row['Category'] . ' : ' . $row['TypeName'] . '</td>';
				echo '<td class="align-right">' . number_format($row['Size'],1) . '&nbsp;' . $row['Unit'] . '</td>';
				echo '<td>' . ($row['ValueType'] == 1 ? 'Calculated' : '') . '</td>';
				echo '<td class="align-right">' . money_format('%.2n', $row['Value']) . '</td>';
				echo '<td class="align-right">' . sprintf("%.1f%%", $adj_total * 100) . '</td>';
				echo '<td>' . $adj_detail . '</td>';
				echo '</tr>';
	        }
			?>
			</tbody>
			<tfoot>
	        <tr>
	          <th></th>
	          <th class="align-right"><?=number_format($total_units,2)?>&nbsp;Ac</th>
			  <th></th>
	          <th class="align-right"><?=money_format('%.1n', $total_value)?></th>
	          <th></th>
			  <th></th>
	        </tr>
			</tfoot>
	      </table>
		</div>
		<hr class="x-clear">

		<div class="x-column x-sm x-1-3">
			<p><b>Tree Growth:</b></p>
			<?=Database_Utils::show('Year 1st Classified', $cama_property->row['TG_YearFirstClassified']);?>
			<?=Database_Utils::ShowDate('Management Plan Date', $cama_property->row['TG_MgmtPlanDate']);?>
		</div>
		<div class="x-column x-sm x-1-3">
			<p><b>Open Space:</b></p>
			<?=Database_Utils::ShowDate('Date Classified', $cama_property->row['OpenSpace_DateClassified']);?>
		</div>
		<div class="x-column x-sm x-1-3">
			<p><b>Farmland:</b></p>
			<?=Database_Utils::ShowDate('Date Classified', $cama_property->row['Farmland_DateClassified']);?>
		</div>
		<hr class="x-clear">
	</div>	<!-- Land Information -->


	<div id="cama-site-details" class="property-box">
		<h5>Site Information</h5>
		<div class="x-column x-sm x-1-1">
			<table class="datatable responsive" data-paging="false" data-info="false" width="100%">
			<thead>
	        <tr>
	          <th>Description</th>
	          <th class="align-right">Adjustment</th>
	        </tr>
			</thead>
			<tbody>
			<?php
			$total_value = 0;
			while ($row = $cama_property->site->fetchArray()) {
				$total_value += $row['SiteImprovementValue'];
				echo '<tr>';
				echo '<td>' . $row['Description'] . '</td>';
				echo '<td class="align-right">' . money_format('%.2n', $row['SiteImprovementValue']) . '</td>';
				echo '</tr>';
	        }
			?>
			</tbody>
			<tfoot>
	        <tr>
	          <th></th>
	          <th class="align-right"><?=money_format('%.2n', $total_value)?></th>
	        </tr>
			</tfoot>
	      </table>
		</div>
		<hr class="x-clear">

		<div class="x-column x-sm x-1-2">
			<p>Lump Sum: <b><?=money_format('%.0n', $cama_property->row["LumpSumAdj"]);?></b></p>
		</div>
		<div class="x-column x-sm x-1-2">
			<?=Database_Utils::show('Road Frontage (in feet)', $cama_property->row['RoadFrontage']);?>
			<?=Database_Utils::show('Water Frontage (in feet)', $cama_property->row['WaterFrontage']);?>
		</div>
		<hr class="x-clear">
	</div>	<!-- Site Information -->


	<div id="cama-primary-building-data" class="property-box">
		<h5>Primary Building Data</h5>
		<div class="x-column x-sm x-1-1">
			<table class="datatable responsive" data-paging="false" data-info="false" width="100%">
			<thead>
	        <tr>
				<th>Building Type</th>
				<th>Area</th>
				<th>Grade</th>
				<th><abbr title="Condition">Cond</abbr></th>
				<th><abbr title="Functional Obsolescence">F.Obs.</abbr></th>
				<th><abbr title="Economic Obsolescence">E.Obs.</abbr></th>
				<th class="align-right">Value</th>
				<th>Color</th>
				<th><abbr title="Year Built">Year</abbr></th>
	        </tr>
			</thead>
			<tbody>
			<?php
			while ($row = $cama_property->building->fetchArray()) {
				echo '<tr>';
				echo '<td>' . $row['CategoryName'] . ($row['Description'] != '' ? ' - <em>' . $row['Description'] . '</em>': '') . '</td>';
				echo '<td>' . $row['Area'] . '</td>';
				echo '<td>' . ($row['Grade'] != '' ? sprintf("%.2f", $row['Grade']) : '') . '</td>';
				echo '<td>' . sprintf("%.0f%%", $row['Condition'] * 100) . '</td>';
				echo '<td>' . sprintf("%.0f%%", $row['FuncObs'] * 100) . '</td>';
				echo '<td>' . sprintf("%.0f%%", $row['EcObs'] * 100) . '</td>';
				echo '<td class="align-right">' . money_format('%.0n', $row['Value']) . '</td>';
				echo '<td>' . $row['Color'] . '</td>';
				echo '<td>' . $row['YearBuilt'] . '</td>';
				echo '</tr>';
	        }
			?>
			</tbody>
	      </table>
		</div>
		<hr class="x-clear">
	</div>	<!-- Primary Building Data -->


	<div id="cama-visit-history" class="property-box">
		<h5>Visit History</h5>
		<div class="x-column x-sm x-1-1">
			<table class="datatable responsive" data-paging="false" data-info="false" width="100%">
			<thead>
	        <tr>
				<th>Date</th>
				<th>Purpose</th>
				<th>Result</th>
				<th>Individual</th>
				<th>Comments</th>
	        </tr>
			</thead>
			<tbody>
			<?php
			while ($row = $cama_property->visit_history->fetchArray()) {
				echo '<tr>';
				echo '<td>' . ($row["VisitDate"] != '' ? date("m/d/Y", strtotime($row["VisitDate"])) : '') . '</td>';
				echo '<td>' . $row['PurposeDesc'] . '</td>';
				echo '<td>' . $row['ResultDesc'] . '</td>';
				echo '<td>' . $row['AssessorName'] . '</td>';
				echo '<td>' . $row['Comments'] . '</td>';
				echo '</tr>';
	        }
			?>
			</tbody>
	      </table>
		</div>
		<hr class="x-clear">
	</div>	<!-- Visit History -->


	<div id="cama-exemptions" class="property-box">
		<h5>Exemptions</h5>
		<div class="x-column x-sm x-1-1">
			<table class="datatable responsive" data-paging="false" data-info="false" width="100%">
			<thead>
	        <tr>
				<th>Type</th>
				<th class="align-right">Value</th>
	        </tr>
			</thead>
			<tbody>
			<?php
			while ($row = $cama_property->exemptions->fetchArray()) {
				echo '<tr>';
				echo '<td>' . $row['ExemptCat'] . '</td>';
				echo '<td class="align-right">' . money_format('%.0n', $row['Value']) . '</td>';
				echo '</tr>';
	        }
			?>
			</tbody>
	      </table>

		</div>
		<hr class="x-clear">
	</div>	<!-- Visit History -->

	<a href="#top">Back to Top</a>


	<?php // print_r($property); ?>




    </div>

	<aside class="x-sidebar right" role="complementary">
		<?php dynamic_sidebar( 'sidebar-cama' ); ?>
	</aside>

  </div>



<?php get_footer(); ?>
