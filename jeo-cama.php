<?php
/*
Plugin Name: JEO CAMA
Plugin URI: http://www.neilarnold.com
Description: Custom Plugin to manage cities and Access Databases
Version: 0.9
Author: Neil Arnold
Author URI: http://www.neilarnold.com
Plugin Type: Piklist
License: GPL2
*/


// Check for Piklist --- --- ---
function checkForPiklist() {
    if(is_admin())   {
        include_once('includes/class-piklist-checker.php');
        if (!piklist_checker::check(__FILE__))
            return;
    }
}
add_action('init', 'checkForPiklist');
// END - Check for Piklist --- --- ---


// Register Custom Post Type --- --- ---
function jeo_init_cpt_cama($post_types)
{
	$post_types['cama'] = array(
		'labels' => piklist('post_type_labels', 'CAMA City')
		,'title' => __('Enter a new City')
		,'public' => true
        ,'rewrite' => array( 'slug' => 'cama' )
		,'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', 'comments', 'excerpt' )
        ,'hide_meta_box' => array( 'slug' , 'author' , 'comments' , 'commentstatus' )
        ,'menu_icon' => 'dashicons-location-alt'
        ,'has_archive' => true
	);

	return $post_types;
}
add_filter('piklist_post_types', 'jeo_init_cpt_cama');
// END - Register Custom Post Type --- --- ---


// Register Custom Template --- --- ---
function jeo_cama_single_template($single_template) {
     global $post;
     if ($post->post_type == 'cama') {
          $single_template = plugin_dir_path(__FILE__ ).'templates/single-cama.php';
     }
     return $single_template;
}
add_filter( "single_template", "jeo_cama_single_template" );
// END - Register Custom Template --- --- ---

// Register Settings Page (Convert Database Page) --- --- ---
  //add_filter('piklist_admin_pages', 'jeo_cama_admin_pages');
  function jeo_cama_admin_pages_DELETE($pages)
  {
    $pages[] = array(
      'page_title' => __('Convert DB')
      ,'menu_title' => __('Convert DB', 'cama')
      ,'sub_menu' => 'edit.php?post_type=cama'
      ,'capability' => 'manage_options'
      ,'menu_slug' => 'jeo_cama_convertdb'
      ,'setting' => 'jeo_cama_convertdb'
      ,'menu_icon' => 'dashicons-share-alt2'
      ,'page_icon' => 'dashicons-share-alt2'
      ,'single_line' => true
      ,'default_tab' => 'Basic'
    );

    return $pages;
  }

add_filter('piklist_admin_pages', 'cama_admin_pages');
function cama_admin_pages($pages)
  {
    $pages[] = array(
      'page_title' => __('Convert Access to SQLite', 'jeo-cama')
      ,'menu_title' => 'Convert DB'
      ,'capability' => 'manage_options'
      ,'menu_slug' => 'jeo_cama_convertdb'
      ,'sub_menu' => 'edit.php?post_type=cama'
      ,'single_line' => false
      ,'menu_icon' => 'dashicons-share-alt2'
      ,'page_icon' => 'dashicons-share-alt2'
    );

	return $pages;
}
// END - Register Settings Page --- --- ---

// Register Custom Sidebar --- --- ---
if ( ! function_exists( 'cama_sidebar' ) ) {
	function cama_sidebar() {
		$args = array(
			'id'            => 'sidebar-cama',
			'name'          => __( 'CAMA', 'text_domain' ),
			'description'   => __( 'Blocks for CAMA Pages', 'text_domain' ),
			'class'         => 'widget-cama',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="h-widget">',
			'after_title'   => '</h4>',
		);
		register_sidebar( $args );
	}
	add_action( 'widgets_init', 'cama_sidebar' );
}
// END - Register Custom Sidebar --- --- ---

// Include JS Library in Cama Template --- --- ---
function myjavascript_in_wp_head($pid){
	global $wp_query;
	if (is_single() || $wp_query->query_vars['city'] != '') {
        global $post;
		if ($post->post_type ==  'cama' || $wp_query->query_vars['city'] != '') {
            wp_enqueue_style( 'datatable', plugin_dir_url( __FILE__ ) . 'includes/js/datatables/css/jquery.dataTables.min.css' );
            wp_enqueue_script( 'datatable', plugin_dir_url( __FILE__ ) . 'includes/js/datatables/js/jquery.dataTables.min.js', array(), '1.0.0', true );
            wp_enqueue_style( 'datatable-r', '//cdn.datatables.net/responsive/1.0.6/css/dataTables.responsive.css' );
            wp_enqueue_script( 'datatable-r', '//cdn.datatables.net/responsive/1.0.6/js/dataTables.responsive.js', array(), '1.0.0', true );

            wp_enqueue_style( 'jeo-cama', plugin_dir_url( __FILE__ ) . 'includes/css/front-end.css' );
            wp_enqueue_script( 'jeo-cama', plugin_dir_url( __FILE__ ) . 'includes/js/front-end.js', array(), '3.1.0', true );

			wp_enqueue_script( 'moment-js', plugin_dir_url( __FILE__ ) . 'includes/js/moment.min.js', array(), '2.10.6', true );

			wp_enqueue_style( 'slick-theme', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.7/slick-theme.min.css' );
			wp_enqueue_style( 'slick-main', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.7/slick.min.css' );
            wp_enqueue_script( 'slick-js', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.7/slick.min.js', array(), '1.5.7', true );

			//wp_enqueue_style( 'slippry', plugin_dir_url( __FILE__ ) . 'includes/js/slippry/slippry.css' );
            //wp_enqueue_script( 'slippry', plugin_dir_url( __FILE__ ) . 'includes/js/slippry/slippry.min.js', array(), '1.0.0', true );

			//wp_enqueue_style( 'owl1', 'https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css' );
			//wp_enqueue_style( 'owl2', 'https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css' );
			//wp_enqueue_style( 'owl3', 'https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.transitions.min.css' );
			//wp_enqueue_script( 'owl4', 'https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js', array(), '1.3.3', true );
        }
    }

}
add_action( 'wp_head', 'myjavascript_in_wp_head' );
// END - Include JS Library in Cama Template --- --- ---


// Move SEO below Piklist --- --- ---
add_filter('wpseo_metabox_prio', 'move_yoast_metabox_to_bottom');
function move_yoast_metabox_to_bottom() { return 'low'; }
// END - Move SEO below Piklist --- --- ---



// Access Database Utils --- --- ---
function load_access_db_utils() {
    if(!is_admin())   {
        include_once('includes/db-utils.php');
		include_once('includes/class-cama-property.php');
		include_once('includes/class-cama-town.php');
    }
}
add_action('init', 'load_access_db_utils');
// END - Check for Piklist --- --- ---

//Sort CPT By Custom Field
function cama_sort_archive_loop($query) {
    if (is_post_type_archive('cama') && in_array ( $query->get('post_type'), array('cama') )) {
        $query->set('order', 'ASC');
        $query->set('orderby', 'title');
    }
}
add_action('pre_get_posts', 'cama_sort_archive_loop');

// Override Breadcrumbs code
if ( ! function_exists( 'x_breadcrumbs' ) ) :
    function x_breadcrumbs() {

        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb('<p id="breadcrumbs">','</p>');
        } else {
            if ( x_get_option( 'x_breadcrumb_display', 1 ) ) {

                GLOBAL $post;

                $is_ltr         = ! is_rtl();
                $stack          = X_STACK;
                $delimiter      = ' <span class="delimiter"><i class="x-icon-angle-' . ( ( $is_ltr ) ? 'right' : 'left' ) . '"></i></span> ';
                $home_text      = '<span class="home"><i class="x-icon-home"></i></span>';
                $home_link      = home_url();
                $current_before = '<span class="current">';
                $current_after  = '</span>';
                $page_title     = get_the_title();
                $blog_title     = get_the_title( get_option( 'page_for_posts', true ) );
                $post_parent    = $post->post_parent;

                if ( function_exists( 'woocommerce_get_page_id' ) ) {
                    $shop_url   = x_get_shop_link();
                    $shop_title = x_get_option( 'x_' . $stack . '_shop_title', __( 'The Shop', '__x__' ) );
                    $shop_link  = '<a href="'. $shop_url .'">' . $shop_title . '</a>';
                }

                echo '<div class="x-breadcrumbs"><a href="' . $home_link . '">' . $home_text . '</a>' . $delimiter;

                if ( is_home() ) {
					global $wp_query;
				    if (isset($wp_query->query_vars['property'])) {
						echo $current_before . 'CAMA' . $current_after;
					} else

                    	echo $current_before . $blog_title . $current_after;

                } elseif ( is_category() ) {

                    $the_cat = get_category( get_query_var( 'cat' ), false );
                    if ( $the_cat->parent != 0 ) echo get_category_parents( $the_cat->parent, TRUE, $delimiter );
                    echo $current_before . single_cat_title( '', false ) . $current_after;

                } elseif ( x_is_product_category() ) {

                    if ( $is_ltr ) {
                        echo $shop_link . $delimiter . $current_before . single_cat_title( '', false ) . $current_after;
                    } else {
                        echo $current_before . single_cat_title( '', false ) . $current_after . $delimiter . $shop_link;
                    }

                } elseif ( x_is_product_tag() ) {

                    if ( $is_ltr ) {
                        echo $shop_link . $delimiter . $current_before . single_tag_title( '', false ) . $current_after;
                    } else {
                        echo $current_before . single_tag_title( '', false ) . $current_after . $delimiter . $shop_link;
                    }

                } elseif ( is_search() ) {

                    echo $current_before . __( 'Search Results for ', '__x__' ) . '�' . get_search_query() . '�' . $current_after;

                } elseif ( is_singular( 'post' ) ) {

                    if ( get_option( 'page_for_posts' ) == is_front_page() ) {
                        echo $current_before . $page_title . $current_after;
                    } else {
                        if ( $is_ltr ) {
                            echo '<a href="' . get_permalink( get_option( 'page_for_posts' ) ) . '">' . $blog_title . '</a>' . $delimiter . $current_before . $page_title . $current_after;
                        } else {
                            echo $current_before . $page_title . $current_after . $delimiter . '<a href="' . get_permalink( get_option( 'page_for_posts' ) ) . '">' . $blog_title . '</a>';
                        }
                    }

                } elseif ( is_singular('cama') ) {

                    echo '<a href="/cama/">CAMA</a>' . $delimiter . $current_before . $page_title . $current_after;


                } elseif ( x_is_portfolio() ) {

                    echo $current_before . get_the_title() . $current_after;

                } elseif ( x_is_portfolio_item() ) {

                    $link  = x_get_parent_portfolio_link();
                    $title = x_get_parent_portfolio_title();

                    if ( $is_ltr ) {
                        echo '<a href="' . $link . '">' . $title . '</a>' . $delimiter . $current_before . $page_title . $current_after;
                    } else {
                        echo $current_before . $page_title . $current_after . $delimiter . '<a href="' . $link . '">' . $title . '</a>';
                    }

                } elseif ( x_is_product() ) {

                    if ( $is_ltr ) {
                        echo $shop_link . $delimiter . $current_before . $page_title . $current_after;
                    } else {
                        echo $current_before . $page_title . $current_after . $delimiter . $shop_link;
                    }

                } elseif ( x_is_buddypress() ) {

                    if ( bp_is_group() ) {
                        echo '<a href="' . bp_get_groups_directory_permalink() . '">' . x_get_option( 'x_buddypress_groups_title', 'Groups' ) . '</a>' . $delimiter . $current_before . x_buddypress_get_the_title() . $current_after;
                    } elseif ( bp_is_user() ) {
                        echo '<a href="' . bp_get_members_directory_permalink() . '">' . x_get_option( 'x_buddypress_members_title', 'Members' ) . '</a>' . $delimiter . $current_before . x_buddypress_get_the_title() . $current_after;
                    } else {
                        echo $current_before . x_buddypress_get_the_title() . $current_after;
                    }

                } elseif ( is_page() && ! $post_parent ) {

                    echo $current_before . $page_title . $current_after;

                } elseif ( is_page() && $post_parent ) {

                    $parent_id   = $post_parent;
                    $breadcrumbs = array();

                    if ( is_rtl() ) {
                        echo $current_before . $page_title . $current_after . $delimiter;
                    }

                    while ( $parent_id ) {
                        $page          = get_page( $parent_id );
                        $breadcrumbs[] = '<a href="' . get_permalink( $page->ID ) . '">' . get_the_title( $page->ID ) . '</a>';
                        $parent_id     = $page->post_parent;
                    }

                    if ( $is_ltr ) {
                        $breadcrumbs = array_reverse( $breadcrumbs );
                    }

                    for ( $i = 0; $i < count( $breadcrumbs ); $i++ ) {
                        echo $breadcrumbs[$i];
                        if ( $i != count( $breadcrumbs ) -1 ) echo $delimiter;
                    }

                    if ( $is_ltr ) {
                        echo $delimiter . $current_before . $page_title . $current_after;
                    }

                } elseif ( is_tag() ) {

                    echo $current_before . single_tag_title( '', false ) . $current_after;

                } elseif ( is_author() ) {

                    GLOBAL $author;
                    $userdata = get_userdata( $author );
                    echo $current_before . __( 'Posts by ', '__x__' ) . '�' . $userdata->display_name . $current_after . '�';

                } elseif ( is_404() ) {

                    echo $current_before . __( '404 (Page Not Found)', '__x__' ) . $current_after;

                } elseif ( is_archive() ) {
					if ( x_is_shop() ) {
                        echo $current_before . $shop_title . $current_after;
                    } else {
                        echo $current_before . __( 'Archives ', '__x__' ) . $current_after;
                    }

                }

                echo '</div>';

            }

        }
    }
endif;

/* Custom URL & Template */
add_filter( 'wp_title', 'rewrite_if_cama_property', 10, 2 );
function rewrite_if_cama_property( $title, $id = null ) {
    global $wp_query;
    if (isset($wp_query->query_vars['property'])) {
		$city_slug = $wp_query->query_vars['city'];
		$city = Database_Utils::get_cama_post($city_slug);
		return $city->post_title;
    }

    return '-' . $title;
}


add_action('admin_init', 'handle_custompage_route');
function handle_custompage_route()
{
    add_rewrite_rule('cama/([^/]+)/([^/]+)/?', 'index.php?city=$matches[1]&property=$matches[2]', 'top');
    flush_rewrite_rules();
}

add_filter('init', 'declare_newsletter_vars');
function declare_newsletter_vars()
{
    add_rewrite_tag('%city%', '([^&]+)');
    add_rewrite_tag('%property%', '([^&]+)');
}

add_filter('template_include', 'my_template', 1, 1);
function my_template($template)
{
    global $wp_query;

    if (isset($wp_query->query_vars['city'])) {
        return dirname(__FILE__) . '/templates/property.php';
    }
    return $template;
}
